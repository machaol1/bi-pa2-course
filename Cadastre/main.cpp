#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */

struct Owner;
struct Land {
	Land(const string & city, const string & addr, const string & region, unsigned int id)
		: m_city(city), m_addr(addr), m_region(region), m_id(id) {}
	~Land() {
		m_owner = NULL;
	}
	static bool cmp_city_addr(const shared_ptr<Land> a, const shared_ptr<Land> b) {
		if (a->m_city == b->m_city && a->m_addr == b->m_addr) return true;
		return false;
	}

	static bool cmp_region_id(const shared_ptr<Land> a, const shared_ptr<Land> b) {
		if (a->m_region == b->m_region && a->m_id == b->m_id) return true;
		return false;
	}

	string m_city, m_addr, m_region, m_ownern = "";
	unsigned int m_id;
	shared_ptr<Owner> m_owner = NULL;
};

struct Owner {
	Owner(const string & name) : o_name(name) {}
	~Owner() {
		lands.clear();
	}
	string o_name;
	vector<shared_ptr<Land>> lands;
};

bool compare_by_city_addr(const shared_ptr<Land> a, const shared_ptr<Land> b) {
	if (a->m_city > b->m_city) return false;
	else if (a->m_city < b->m_city) return true;
	else {
		if (a->m_addr > b->m_addr) return false;
		else if (a->m_addr < b->m_addr) return true;
		else return true;
	}
}

bool compare_by_region_id(const shared_ptr<Land> a, const shared_ptr<Land> b) {
	if (a->m_id > b->m_id) return false;
	else if (a->m_id < b->m_id) return true;
	else {
		if (a->m_region > b->m_region) return false;
		else if (a->m_region < b->m_region) return true;
		else return true;
	}
}

bool cmp_c_a(const shared_ptr<Land> a, const shared_ptr<Land> b) {
	if (a->m_city == b->m_city && a->m_addr == b->m_addr) return true;
	return false;
}

bool cmp_r_id(const shared_ptr<Land> a, const shared_ptr<Land> b) {
	if (a->m_region == b->m_region && a->m_id == b->m_id) return true;
	return false;
}

string stoupper(const string in) {
	string out = in;
	for (size_t i = 0; i < in.size(); i++) {
		out[i] = toupper(in[i]);
	}
	return out;
}

bool compare_by_owner(const shared_ptr<Owner> a, const shared_ptr<Owner> b) {

	if (stoupper(a->o_name) > stoupper(b->o_name)) return false;
	if (stoupper(a->o_name) < stoupper(b->o_name)) return true;
	return true;
}




class CIterator
{
public:
	CIterator() {
		fail = 1;
	}

	CIterator(const vector<shared_ptr<Land>> & in) {
		current = in.begin();
		end = in.end();
	}

	bool                     AtEnd(void) const {
		if (fail == 0 && current != end) return false;
		return true;
	}
	void                     Next(void) {
		if (!AtEnd()) ++current;
	}
	string                   City(void) const {
		if (!AtEnd()) return (*current)->m_city;
		return "";
	}
	string                   Addr(void) const {
		if (!AtEnd()) return (*current)->m_addr;
		return "";
	}
	string                   Region(void) const {
		if (!AtEnd()) return  (*current)->m_region;
		return "";
	}
	unsigned                 ID(void) const {
		if (!AtEnd()) return  (*current)->m_id;
		return 0;
	}
	string                   Owner(void) const {
		if (!AtEnd()) return  (*current)->m_ownern;
		return "";
	}
private:
	vector<shared_ptr<Land>>::const_iterator current;
	vector<shared_ptr<Land>>::const_iterator end;
	int fail = 0;
};

class CLandRegister
{
public:
	CLandRegister() {
		shared_ptr<Owner> tmp = make_shared<Owner>("");
		OWNER_DB.push_back(tmp);
	}

	~CLandRegister() {
		CITY_ADDR_DB.clear();
		REGION_ID_DB.clear();
		for (size_t i = 0; i < OWNER_DB.size(); i++) {
			OWNER_DB[i]->lands.clear();
		}
		OWNER_DB.clear();
	}

	bool                     Add(const string    & city,
		const string    & addr,
		const string    & region,
		unsigned int      id) {

		shared_ptr<Land> tmp = make_shared<Land>(city, addr, region, id);
		auto it1 = lower_bound(CITY_ADDR_DB.begin(), CITY_ADDR_DB.end(), tmp, compare_by_city_addr);
		auto it2 = lower_bound(REGION_ID_DB.begin(), REGION_ID_DB.end(), tmp, compare_by_region_id);

		if (it1 != CITY_ADDR_DB.begin()) if (cmp_c_a(*prev(it1), tmp)) return false;
		if (it2 != REGION_ID_DB.begin()) if (cmp_r_id(*prev(it2), tmp)) return false;

		CITY_ADDR_DB.insert(it1, tmp);
		REGION_ID_DB.insert(it2, tmp);

		OWNER_DB[0]->lands.push_back(tmp);
		tmp->m_owner = OWNER_DB[0];
		tmp->m_ownern = "";
		return true;
	}

	bool                     Del(const string    & city,
		const string    & addr) {
		shared_ptr<Land> tmp = make_shared<Land>(city, addr, "", 0);

		auto it1 = lower_bound(CITY_ADDR_DB.begin(), CITY_ADDR_DB.end(), tmp, compare_by_city_addr);
		if (it1 == CITY_ADDR_DB.begin()) return false;
		if (!cmp_c_a(*prev(it1), tmp)) return false;

		tmp->m_region = (*prev(it1))->m_region;
		tmp->m_id = (*prev(it1))->m_id;
		auto it2 = lower_bound(REGION_ID_DB.begin(), REGION_ID_DB.end(), tmp, compare_by_region_id);
		if (it2 == REGION_ID_DB.begin()) return false;
		if (!cmp_r_id(*prev(it2), tmp)) return false;

		for (auto i = (*prev(it1))->m_owner->lands.begin(); i != (*prev(it1))->m_owner->lands.end(); ++i) {
			if ((*i)->m_id == tmp->m_id && (*i)->m_region == tmp->m_region) {
				(*prev(it1))->m_owner->lands.erase(i);
				break;
			}
		}

		CITY_ADDR_DB.erase(prev(it1));
		REGION_ID_DB.erase(prev(it2));
		return true;
	}

	bool                     Del(const string    & region,
		unsigned int      id) {
		shared_ptr<Land> tmp = make_shared<Land>("", "", region, id);

		auto it1 = lower_bound(REGION_ID_DB.begin(), REGION_ID_DB.end(), tmp, compare_by_region_id);
		if (it1 == REGION_ID_DB.begin()) return false;
		if (!cmp_r_id(*prev(it1), tmp)) return false;

		tmp->m_city = (*prev(it1))->m_city;
		tmp->m_addr = (*prev(it1))->m_addr;
		auto it2 = lower_bound(CITY_ADDR_DB.begin(), CITY_ADDR_DB.end(), tmp, compare_by_city_addr);
		if (it2 == CITY_ADDR_DB.begin()) return false;
		if (!cmp_c_a(*prev(it2), tmp)) return false;

		for (auto i = (*prev(it1))->m_owner->lands.begin(); i != (*prev(it1))->m_owner->lands.end(); ++i) {
			if ((*i)->m_id == tmp->m_id && (*i)->m_region == tmp->m_region) {
				(*prev(it1))->m_owner->lands.erase(i);
				break;
			}
		}

		REGION_ID_DB.erase(prev(it1));
		CITY_ADDR_DB.erase(prev(it2));
		return true;
	}

	bool                     GetOwner(const string    & city,
		const string    & addr,
		string          & owner) const {
		shared_ptr<Land> tmp = make_shared<Land>(city, addr, "", 0);

		auto it1 = lower_bound(CITY_ADDR_DB.begin(), CITY_ADDR_DB.end(), tmp, compare_by_city_addr);
		if (it1 == CITY_ADDR_DB.begin()) return false;
		if (cmp_c_a(*prev(it1), tmp)) {
			owner = (*prev(it1))->m_ownern;
			return true;
		}
		return false;
	}

	bool                     GetOwner(const string    & region,
		unsigned int      id,
		string          & owner) const {
		shared_ptr<Land> tmp = make_shared<Land>("", "", region, id);

		auto it1 = lower_bound(REGION_ID_DB.begin(), REGION_ID_DB.end(), tmp, compare_by_region_id);
		if (it1 == REGION_ID_DB.begin()) return false;
		if (cmp_r_id(*prev(it1), tmp)) {
			owner = (*prev(it1))->m_ownern;
			return true;
		}
		return false;
	}

	bool                     NewOwner(const string    & city,
		const string    & addr,
		const string    & owner) {

		shared_ptr<Land> tmp1 = make_shared<Land>(city, addr, "", 0);
		auto it1 = lower_bound(CITY_ADDR_DB.begin(), CITY_ADDR_DB.end(), tmp1, compare_by_city_addr);
		if (it1 == CITY_ADDR_DB.begin()) return false;
		if (!cmp_c_a(*prev(it1), tmp1)) return false;
		if (stoupper((*prev(it1))->m_ownern) == stoupper(owner)) return false;

		shared_ptr<Owner> tmp2 = make_shared<Owner>(owner);
		auto it2 = lower_bound(OWNER_DB.begin(), OWNER_DB.end(), tmp2, compare_by_owner);

		if (it2 == OWNER_DB.begin() || stoupper(owner) != stoupper((*prev(it2))->o_name)) {

			for (auto i = (*prev(it1))->m_owner->lands.begin(); i != (*prev(it1))->m_owner->lands.end(); ++i) {
				if ((*i)->m_city == city && (*i)->m_addr == addr) {
					(*prev(it1))->m_owner->lands.erase(i);
					break;
				}
			}

			tmp2->lands.push_back(*prev(it1));
			(*prev(it1))->m_owner = tmp2;
			(*prev(it1))->m_ownern = owner;

			OWNER_DB.insert(it2, tmp2);
			return true;
		}

		for (auto i = (*prev(it1))->m_owner->lands.begin(); i != (*prev(it1))->m_owner->lands.end(); ++i) {
			if ((*i)->m_city == city && (*i)->m_addr == addr) {
				(*prev(it1))->m_owner->lands.erase(i);
				break;
			}
		}

		(*prev(it1))->m_owner = *prev(it2);
		(*prev(it1))->m_ownern = owner;
		(*prev(it2))->lands.push_back(*prev(it1));
		return true;
	}

	bool                     NewOwner(const string    & region,
		unsigned int      id,
		const string    & owner) {

		shared_ptr<Land> tmp1 = make_shared<Land>("", "", region, id);
		auto it1 = lower_bound(REGION_ID_DB.begin(), REGION_ID_DB.end(), tmp1, compare_by_region_id);
		if (it1 == REGION_ID_DB.begin()) return false;
		if (!cmp_r_id(*prev(it1), tmp1)) return false;
		if (stoupper((*prev(it1))->m_owner->o_name) == stoupper(owner)) return false;

		shared_ptr<Owner> tmp2 = make_shared<Owner>(owner);
		auto it2 = lower_bound(OWNER_DB.begin(), OWNER_DB.end(), tmp2, compare_by_owner);
		if (it2 == OWNER_DB.begin() || stoupper(owner) != stoupper((*prev(it2))->o_name)) {
			for (auto i = (*prev(it1))->m_owner->lands.begin(); i != (*prev(it1))->m_owner->lands.end(); ++i) {
				if ((*i)->m_region == region && (*i)->m_id == id) {
					(*prev(it1))->m_owner->lands.erase(i);
					break;
				}
			}

			tmp2->lands.push_back(*prev(it1));
			(*prev(it1))->m_owner = tmp2;
			(*prev(it1))->m_ownern = owner;

			OWNER_DB.insert(it2, tmp2);
			return true;
		}

		for (auto i = (*prev(it1))->m_owner->lands.begin(); i != (*prev(it1))->m_owner->lands.end(); ++i) {
			if ((*i)->m_region == region && (*i)->m_id == id) {
				(*prev(it1))->m_owner->lands.erase(i);
				break;
			}
		}

		(*prev(it1))->m_owner = *prev(it2);
		(*prev(it1))->m_ownern = owner;
		(*prev(it2))->lands.push_back(*prev(it1));
		return true;
	}

	unsigned                 Count(const string    & owner) const {
		shared_ptr<Owner> tmp = make_shared<Owner>(owner);
		auto it1 = lower_bound(OWNER_DB.begin(), OWNER_DB.end(), tmp, compare_by_owner);
		if (it1 == OWNER_DB.begin()) return 0;
		if (stoupper(tmp->o_name) == stoupper((*prev(it1))->o_name)) return (*prev(it1))->lands.size();
		return 0;
	}

	CIterator                ListByAddr(void) const { // const
		CIterator m(CITY_ADDR_DB);
		return CIterator(CITY_ADDR_DB);
	}

	CIterator                ListByOwner(const string    & owner) const { //const
		shared_ptr<Owner> tmp = make_shared<Owner>(owner);
		auto it1 = lower_bound(OWNER_DB.begin(), OWNER_DB.end(), tmp, compare_by_owner);
		if (it1 == OWNER_DB.begin()) return CIterator();
		if (stoupper(owner) == stoupper((*prev(it1))->o_name)) return CIterator((*prev(it1))->lands);
		return CIterator();
	}
private:

	vector<shared_ptr<Land>> CITY_ADDR_DB;
	vector<shared_ptr<Land>> REGION_ID_DB;
	vector<shared_ptr<Owner>> OWNER_DB;
};
#ifndef __PROGTEST__

static void test3(void) {
	CLandRegister x;
	string owner;

	assert(x.Count("") == 0);
	assert(x.Del("Donetsk", 12) == false);
	assert(x.Del("asd", "sads") == false);
	CIterator i0 = x.ListByAddr();
	assert(i0.AtEnd());

	i0 = x.ListByOwner("");
	assert(i0.AtEnd());
	i0 = x.ListByOwner("1");
	assert(i0.AtEnd());
	assert(x.GetOwner("asd", 1, owner) == false);
	assert(x.GetOwner("asd", "asddad", owner) == false);
	assert(x.NewOwner("asd", "dsada", "lol") == false);
	assert(x.Add("lol", "lol", "lol", 1) == true);
	assert(x.Add("lol", "lol", "lol", 1) == false);
	assert(x.GetOwner("lol", "lol", owner) && owner == "");
	assert(x.GetOwner("lol", 1, owner) && owner == "");
	assert(x.NewOwner("lol", "lol", "") == false);
	assert(x.NewOwner("lol", 1, "") == false);
	assert(x.Count("1111111") == 0);
	assert(x.Count("") == 1);
	assert(x.NewOwner("lol", 1, "Ihor") == true);
	assert(x.GetOwner("lol", "lol", owner) && owner == "Ihor");
	assert(x.GetOwner("lol", 1, owner) && owner == "Ihor");
	assert(x.Count("Ihor") == 1);
	assert(x.Count("IHOR") == 1);
	assert(x.Count("") == 0);

}

static void test2(void) {
	CLandRegister x;
	string owner;

	assert(x.Add("Donetsk", "Artema", "Kievskij", 83004));
	assert(x.Add("Donetsk", "Artema", "Kalininskiy", 83005) == false);
	assert(x.Add("Makeevka", "Lenina", "Kievskij", 83004) == false);
	assert(x.Add("Makeevka", "Lenina", "Kievskij", 83005));
	assert(x.Count("") == 2);
	assert(x.GetOwner("Donetsk", "Artema", owner) && owner == "");
	assert(x.NewOwner("Donetsk", "Artema", "Ihor"));
	assert(x.NewOwner("Kievskij", 83004, "Ihor") == false);
	assert(x.Count("") == 1);
	assert(x.Count("Ihor") == 1);
	CIterator i0 = x.ListByOwner("");
	int a = 0;
	while (!i0.AtEnd()) {
		a++;
		i0.Next();
	}
	assert(a == 1);

	CIterator i1 = x.ListByOwner("IHOR");
	a = 0;
	while (!i1.AtEnd()) {
		a++;
		i1.Next();
	}
	assert(a == 1);

	assert(x.NewOwner("Makeevka", "Lenina", "IHOR"));

	CIterator i2 = x.ListByOwner("IHOR");
	a = 0;
	while (!i2.AtEnd()) {
		a++;
		i2.Next();
	}
	assert(a == 2);

	CIterator i3 = x.ListByOwner("");
	a = 0;
	while (!i3.AtEnd()) {
		a++;
		i3.Next();
	}

	assert(a == 0);

}


static void test0(void)
{

	CLandRegister x;
	string owner;

	assert(x.Add("Prague", "Thakurova", "Dejvice", 12345));
	assert(x.Add("Prague", "Evropska", "Vokovice", 12345));
	assert(x.Add("Prague", "Technicka", "Dejvice", 9873));
	assert(x.Add("Plzen", "Evropska", "Plzen mesto", 78901));
	assert(x.Add("Liberec", "Evropska", "Librec", 4552));



	CIterator i0 = x.ListByAddr();


	assert(!i0.AtEnd()
		&& i0.City() == "Liberec"
		&& i0.Addr() == "Evropska"
		&& i0.Region() == "Librec"
		&& i0.ID() == 4552
		&& i0.Owner() == "");
	i0.Next();
	assert(!i0.AtEnd()
		&& i0.City() == "Plzen"
		&& i0.Addr() == "Evropska"
		&& i0.Region() == "Plzen mesto"
		&& i0.ID() == 78901
		&& i0.Owner() == "");

	i0.Next();
	assert(!i0.AtEnd()
		&& i0.City() == "Prague"
		&& i0.Addr() == "Evropska"
		&& i0.Region() == "Vokovice"
		&& i0.ID() == 12345
		&& i0.Owner() == "");
	i0.Next();
	assert(!i0.AtEnd()
		&& i0.City() == "Prague"
		&& i0.Addr() == "Technicka"
		&& i0.Region() == "Dejvice"
		&& i0.ID() == 9873
		&& i0.Owner() == "");
	i0.Next();
	assert(!i0.AtEnd()
		&& i0.City() == "Prague"
		&& i0.Addr() == "Thakurova"
		&& i0.Region() == "Dejvice"
		&& i0.ID() == 12345
		&& i0.Owner() == "");
	i0.Next();
	assert(i0.AtEnd());
	assert(x.Count("") == 5);

	CIterator i1 = x.ListByOwner("");
	assert(!i1.AtEnd()
		&& i1.City() == "Prague"
		&& i1.Addr() == "Thakurova"
		&& i1.Region() == "Dejvice"
		&& i1.ID() == 12345
		&& i1.Owner() == "");
	i1.Next();
	assert(!i1.AtEnd()
		&& i1.City() == "Prague"
		&& i1.Addr() == "Evropska"
		&& i1.Region() == "Vokovice"
		&& i1.ID() == 12345
		&& i1.Owner() == "");
	i1.Next();
	assert(!i1.AtEnd()
		&& i1.City() == "Prague"
		&& i1.Addr() == "Technicka"
		&& i1.Region() == "Dejvice"
		&& i1.ID() == 9873
		&& i1.Owner() == "");
	i1.Next();
	assert(!i1.AtEnd()
		&& i1.City() == "Plzen"
		&& i1.Addr() == "Evropska"
		&& i1.Region() == "Plzen mesto"
		&& i1.ID() == 78901
		&& i1.Owner() == "");
	i1.Next();
	assert(!i1.AtEnd()
		&& i1.City() == "Liberec"
		&& i1.Addr() == "Evropska"
		&& i1.Region() == "Librec"
		&& i1.ID() == 4552
		&& i1.Owner() == "");
	i1.Next();
	assert(i1.AtEnd());

	assert(x.Count("CVUT") == 0);
	CIterator i2 = x.ListByOwner("CVUT");
	assert(i2.AtEnd());

	assert(x.NewOwner("Prague", "Thakurova", "CVUT"));
	assert(x.NewOwner("Donetsk", "T", "CVUT") == false);
	assert(x.NewOwner("Dejvice", 9873, "CVUT"));
	assert(x.NewOwner("Plzen", "Evropska", "Anton Hrabis"));
	assert(x.NewOwner("Librec", 4552, "Cvut"));

	assert(x.GetOwner("Liberec", "Evropska", owner) && owner == "Cvut");
	assert(x.GetOwner("Librec", 4552, owner) && owner == "Cvut");

	assert(x.GetOwner("Prague", "Thakurova", owner) && owner == "CVUT");
	assert(x.GetOwner("Dejvice", 12345, owner) && owner == "CVUT");
	assert(x.GetOwner("Prague", "Evropska", owner) && owner == "");
	assert(x.GetOwner("Vokovice", 12345, owner) && owner == "");
	assert(x.GetOwner("Prague", "Technicka", owner) && owner == "CVUT");
	assert(x.GetOwner("Dejvice", 9873, owner) && owner == "CVUT");
	assert(x.GetOwner("Plzen", "Evropska", owner) && owner == "Anton Hrabis");
	assert(x.GetOwner("Plzen mesto", 78901, owner) && owner == "Anton Hrabis");

	CIterator i3 = x.ListByAddr();
	assert(!i3.AtEnd()
		&& i3.City() == "Liberec"
		&& i3.Addr() == "Evropska"
		&& i3.Region() == "Librec"
		&& i3.ID() == 4552
		&& i3.Owner() == "Cvut");
	i3.Next();
	assert(!i3.AtEnd()
		&& i3.City() == "Plzen"
		&& i3.Addr() == "Evropska"
		&& i3.Region() == "Plzen mesto"
		&& i3.ID() == 78901
		&& i3.Owner() == "Anton Hrabis");
	i3.Next();
	assert(!i3.AtEnd()
		&& i3.City() == "Prague"
		&& i3.Addr() == "Evropska"
		&& i3.Region() == "Vokovice"
		&& i3.ID() == 12345
		&& i3.Owner() == "");
	i3.Next();
	assert(!i3.AtEnd()
		&& i3.City() == "Prague"
		&& i3.Addr() == "Technicka"
		&& i3.Region() == "Dejvice"
		&& i3.ID() == 9873
		&& i3.Owner() == "CVUT");
	i3.Next();
	assert(!i3.AtEnd()
		&& i3.City() == "Prague"
		&& i3.Addr() == "Thakurova"
		&& i3.Region() == "Dejvice"
		&& i3.ID() == 12345
		&& i3.Owner() == "CVUT");
	i3.Next();
	assert(i3.AtEnd());

	assert(x.Count("cvut") == 3);
	CIterator i4 = x.ListByOwner("cVuT");
	assert(!i4.AtEnd()
		&& i4.City() == "Prague"
		&& i4.Addr() == "Thakurova"
		&& i4.Region() == "Dejvice"
		&& i4.ID() == 12345
		&& i4.Owner() == "CVUT");
	i4.Next();
	assert(!i4.AtEnd()
		&& i4.City() == "Prague"
		&& i4.Addr() == "Technicka"
		&& i4.Region() == "Dejvice"
		&& i4.ID() == 9873
		&& i4.Owner() == "CVUT");
	i4.Next();
	assert(!i4.AtEnd()
		&& i4.City() == "Liberec"
		&& i4.Addr() == "Evropska"
		&& i4.Region() == "Librec"
		&& i4.ID() == 4552
		&& i4.Owner() == "Cvut");
	i4.Next();
	assert(i4.AtEnd());

	assert(x.NewOwner("Plzen mesto", 78901, "CVut"));
	assert(x.Count("CVUT") == 4);
	CIterator i5 = x.ListByOwner("CVUT");
	assert(!i5.AtEnd()
		&& i5.City() == "Prague"
		&& i5.Addr() == "Thakurova"
		&& i5.Region() == "Dejvice"
		&& i5.ID() == 12345
		&& i5.Owner() == "CVUT");
	i5.Next();
	assert(!i5.AtEnd()
		&& i5.City() == "Prague"
		&& i5.Addr() == "Technicka"
		&& i5.Region() == "Dejvice"
		&& i5.ID() == 9873
		&& i5.Owner() == "CVUT");
	i5.Next();
	assert(!i5.AtEnd()
		&& i5.City() == "Liberec"
		&& i5.Addr() == "Evropska"
		&& i5.Region() == "Librec"
		&& i5.ID() == 4552
		&& i5.Owner() == "Cvut");
	i5.Next();
	assert(!i5.AtEnd()
		&& i5.City() == "Plzen"
		&& i5.Addr() == "Evropska"
		&& i5.Region() == "Plzen mesto"
		&& i5.ID() == 78901
		&& i5.Owner() == "CVut");
	i5.Next();
	assert(i5.AtEnd());

	assert(x.Del("Liberec", "Evropska"));
	assert(x.Del("Plzen mesto", 78901));
	assert(x.Count("cvut") == 2);
	CIterator i6 = x.ListByOwner("cVuT");
	assert(!i6.AtEnd()
		&& i6.City() == "Prague"
		&& i6.Addr() == "Thakurova"
		&& i6.Region() == "Dejvice"
		&& i6.ID() == 12345
		&& i6.Owner() == "CVUT");
	i6.Next();
	assert(!i6.AtEnd()
		&& i6.City() == "Prague"
		&& i6.Addr() == "Technicka"
		&& i6.Region() == "Dejvice"
		&& i6.ID() == 9873
		&& i6.Owner() == "CVUT");
	i6.Next();
	assert(i6.AtEnd());

	assert(x.Add("Liberec", "Evropska", "Librec", 4552));
}

static void test1(void)
{
	CLandRegister x;
	string owner;

	assert(x.Add("Prague", "Thakurova", "Dejvice", 12345));
	assert(x.Add("Prague", "Evropska", "Vokovice", 12345));
	assert(x.Add("Prague", "Technicka", "Dejvice", 9873));
	assert(!x.Add("Prague", "Technicka", "Hradcany", 7344));
	assert(!x.Add("Brno", "Bozetechova", "Dejvice", 9873));
	assert(!x.GetOwner("Prague", "THAKUROVA", owner));
	assert(!x.GetOwner("Hradcany", 7343, owner));
	CIterator i0 = x.ListByAddr();
	assert(!i0.AtEnd()
		&& i0.City() == "Prague"
		&& i0.Addr() == "Evropska"
		&& i0.Region() == "Vokovice"
		&& i0.ID() == 12345
		&& i0.Owner() == "");
	i0.Next();
	assert(!i0.AtEnd()
		&& i0.City() == "Prague"
		&& i0.Addr() == "Technicka"
		&& i0.Region() == "Dejvice"
		&& i0.ID() == 9873
		&& i0.Owner() == "");
	i0.Next();
	assert(!i0.AtEnd()
		&& i0.City() == "Prague"
		&& i0.Addr() == "Thakurova"
		&& i0.Region() == "Dejvice"
		&& i0.ID() == 12345
		&& i0.Owner() == "");
	i0.Next();
	assert(i0.AtEnd());

	assert(x.NewOwner("Prague", "Thakurova", "CVUT"));
	assert(!x.NewOwner("Prague", "technicka", "CVUT"));
	assert(!x.NewOwner("prague", "Technicka", "CVUT"));
	assert(!x.NewOwner("dejvice", 9873, "CVUT"));
	assert(!x.NewOwner("Dejvice", 9973, "CVUT"));
	assert(!x.NewOwner("Dejvice", 12345, "CVUT"));
	assert(x.Count("CVUT") == 1);
	CIterator i1 = x.ListByOwner("CVUT");
	assert(!i1.AtEnd()
		&& i1.City() == "Prague"
		&& i1.Addr() == "Thakurova"
		&& i1.Region() == "Dejvice"
		&& i1.ID() == 12345
		&& i1.Owner() == "CVUT");
	i1.Next();
	assert(i1.AtEnd());

	assert(!x.Del("Brno", "Technicka"));
	assert(!x.Del("Karlin", 9873));
	assert(x.Del("Prague", "Technicka"));
	assert(!x.Del("Prague", "Technicka"));
	assert(!x.Del("Dejvice", 9873));

}


int main(void)
{

	test0();
	test1();
	test2();
	test3();

	return 0;
}
#endif /* __PROGTEST__ */
