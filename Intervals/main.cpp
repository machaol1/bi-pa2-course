#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <ctime>
#include <climits>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
using namespace std;
class InvalidRangeException
{
};
#endif /* __PROGTEST__ */

class CRange;
bool cmp(const CRange & a, const CRange & b);

class CRange
{
public:

	CRange(const long long & lo, const long long & hi) {
		if (lo <= hi) {
			m_lo = lo;
			m_hi = hi;
		}
		else throw InvalidRangeException();
	}

	vector<CRange> operator + (const CRange & range) const {
		vector<CRange> result;
		if (range.m_lo <= this->m_lo) {
			if (range.m_hi <= this->m_hi) {
				if (this->m_lo == LLONG_MIN) {
					result.push_back(CRange(range.m_lo, this->m_hi));
				}
				else if (range.m_hi < this->m_lo - 1) {
					result.push_back(CRange(range.m_lo, range.m_hi));
					result.push_back(CRange(this->m_lo, this->m_hi));
				}
				else result.push_back(CRange(range.m_lo, this->m_hi));
			}
			else result.push_back(CRange(range.m_lo, range.m_hi));
		}
		else {
			if (this->m_hi == LLONG_MAX) {
				result.push_back(CRange(this->m_lo, this->m_hi));
			}
			else if (range.m_lo > this->m_hi + 1) {
				result.push_back(CRange(this->m_lo, this->m_hi));
				result.push_back(CRange(range.m_lo, range.m_hi));
			}
			else {
				if (range.m_hi <= this->m_hi) result.push_back(CRange(this->m_lo, this->m_hi));
				else result.push_back(CRange(this->m_lo, range.m_hi));
			}
		}
		return result;
	}

	friend vector<CRange> operator + (const vector<CRange> & list, const CRange & range) {
		vector<CRange> result = list;
		auto it = lower_bound(result.begin(), result.end(), range, cmp);
		if (it == result.end()) {
			result.insert(it, range);
			return result;
		}

		if (range.m_lo >= it->m_lo) {
			if (it->m_hi == LLONG_MAX || range.m_lo <= it->m_hi + 1) {
				vector<CRange> tmp = *it + range;
				*it = tmp[0];
			}
			else it = result.insert(it, range);
		}
		else it = result.insert(it, range);

		auto itt = it;
		if (itt != result.end()) {
			itt++;

			while (itt != result.end()) {
				if (it->m_hi >= itt->m_lo - 1) {
					vector<CRange> tmp = *itt + *it;
					*it = tmp[0];
					itt = result.erase(itt);
				}
				else break;
			}
		}
		return result;
	}


	vector<CRange> operator - (const CRange & range) const {
		vector<CRange> result;
		if (range.m_lo <= this->m_lo) {
			if (range.m_hi < this->m_hi) {
				if (range.m_hi < this->m_lo) result.push_back(CRange(this->m_lo, this->m_hi));
				else result.push_back(CRange(range.m_hi + 1, this->m_hi));
			}
			else {}
		}
		else {
			if (range.m_lo > this->m_hi) result.push_back(CRange(this->m_lo, this->m_hi));
			else {
				if (range.m_hi < this->m_hi) {
					result.push_back(CRange(this->m_lo, range.m_lo - 1));
					result.push_back(CRange(range.m_hi + 1, this->m_hi));
				}
				else result.push_back(CRange(this->m_lo, range.m_lo - 1));
			}
		}
		return result;
	}

	friend vector<CRange> operator - (const vector<CRange> & list, const CRange & range) {
		vector<CRange> result = list;
		auto it = lower_bound(result.begin(), result.end(), range, cmp);
		if (it == result.end()) return result;
		if (range.m_lo > it->m_lo && range.m_lo <= it->m_hi) {
			vector<CRange> tmp = *it - range;
			if (tmp.size() == 2) {
				*it = tmp[0];
				it = result.insert(++it, tmp[1]);
			}
			else *it = tmp[0];
		}
		else {
			vector<CRange> tmp = *it - range;
			if (tmp.size() == 0) it = result.erase(it);
			else if (tmp.size() == 1) *it = tmp[0];
			else {
				*it = tmp[0];
				it = result.insert(++it, tmp[1]);
			}
		}

		auto itt = it;
		if (itt != result.end()) {
			itt++;

			while (itt != result.end()) {
				if (range.m_hi >= itt->m_lo) {
					vector<CRange> tmp = *itt - range;
					if (tmp.size() == 2) {
						*itt = tmp[0];
						itt = result.insert(it, tmp[1]);
					}
					else if (tmp.size() == 1) *itt = tmp[0];
					else itt = result.erase(itt);
				}
				else break;
			}
		}
		return result;
	}

	bool operator == (const CRange & a) const {
		return (this->m_lo == a.m_lo && this->m_hi == a.m_hi);
	}

	bool operator != (const CRange & a) const {
		return !(*this == a);
	}

	long long m_lo, m_hi;
};

bool cmp(const CRange & a, const CRange & b) {
	if (a.m_hi == LLONG_MAX) return false;
	if (b.m_lo > a.m_hi + 1) return true;
	return false;
}


class CRangeList
{
public:
	CRangeList() {}
	CRangeList(const CRangeList & list) {
		m_Data = list.m_Data;
	}

	bool Includes(int range) const {
		return Includes(CRange(range, range));
	}

	bool Includes(const long long & range) const {
		return Includes(CRange(range, range));
	}

	bool Includes(const CRange & range) const {
		auto it = lower_bound(m_Data.begin(), m_Data.end(), range, cmp);
		if (it == m_Data.end()) return false;
		if (range.m_lo >= it->m_lo && range.m_hi <= it->m_hi) return true;
		return false;
	}

	CRangeList & operator += (const CRange & range) {
		vector<CRange> tmp = m_Data + range;
		m_Data = tmp;
		return *this;
	}

	CRangeList & operator += (const vector<CRange> & vec) {
		for (size_t i = 0; i < vec.size(); i++) {
			*this = *this += vec[i];
		}
		return *this;
	}

	CRangeList & operator += (const CRangeList & list) {
		for (size_t i = 0; i < list.m_Data.size(); i++) {
			*this = *this += list.m_Data[i];
		}
		return *this;
	}

	CRangeList & operator -= (const CRange & range) {
		vector<CRange> tmp = m_Data - range;
		m_Data = tmp;
		return *this;
	}

	CRangeList & operator -= (const vector<CRange> & vec) {
		for (size_t i = 0; i < vec.size(); i++) {
			*this = *this -= vec[i];
		}
		return *this;
	}

	CRangeList & operator -= (const CRangeList & list) {
		*this = *this -= list.m_Data;
		return *this;
	}

	CRangeList & operator = (const CRange & range) {
		m_Data.clear();
		m_Data.push_back(range);
		return *this;
	}

	CRangeList & operator = (const vector<CRange> & list) {
		m_Data = list;
		return *this;
	}

	CRangeList & operator = (const CRangeList & list) {
		m_Data = list.m_Data;
		return *this;
	}

	bool operator == (const CRangeList & list) const {
		if (m_Data.size() != list.m_Data.size()) return false;
		for (size_t i = 0; i < m_Data.size(); i++) {
			if (m_Data[i] != list.m_Data[i]) return false;
		}
		return true;
	}

	bool operator != (const CRangeList & list) const {
		return !(*this == list);
	}

	friend ostream& operator<< (std::ostream &out, const CRangeList &list) {
		out << "{";
		for (size_t i = 0; i < list.m_Data.size(); i++) {
			if (i != 0) out << ",";
			if (list.m_Data[i].m_lo == list.m_Data[i].m_hi) out << list.m_Data[i].m_lo;
			else out << "<" << list.m_Data[i].m_lo << ".." << list.m_Data[i].m_hi << ">";
		}
		out << "}";
		return out;
	}

	vector<CRange> m_Data;
};