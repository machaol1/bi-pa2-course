#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
using namespace std;
#endif /* __PROGTEST__ */

class CComponent;
class CComputer;
class CNetwork;
class CCPU;
class CMemory;
class CDisk;


/*
	Abstract class CComponent represents
	general PC component
*/

class CComponent
{
public:
	virtual void print(ostream & os, int first, bool second) const = 0;
	virtual shared_ptr<CComponent> copy() const = 0;
};



/*
	Class CComputer represents
	one computer
*/

class CComputer
{
public:
	// Constructor
	CComputer(const string & name) : m_name(name) {}

	// Copy constructor
	CComputer(const CComputer & comp)
	{
		m_name = comp.m_name;
		m_adresses = comp.m_adresses;
		for (size_t i = 0; i < comp.m_components.size(); i++)
		{
			m_components.push_back(comp.m_components[i]->copy());
		}
	}

	//Operator =
	CComputer & operator = (const CComputer & comp)
	{
		if (this == &comp) return *this;

		m_name = comp.m_name;
		m_adresses = comp.m_adresses;

		m_components.clear();
		for (size_t i = 0; i < comp.m_components.size(); i++)
		{
			m_components.push_back(comp.m_components[i]->copy());
		}

		return *this;
	}

	// Adds new component to the computer
	CComputer & AddComponent(const CComponent & component)
	{
		shared_ptr<CComponent> ptr = component.copy();
		m_components.push_back(ptr);
		return *this;
	}

	// Adds new ip address to the current computer
	CComputer & AddAddress(const string & ip)
	{
		m_adresses.push_back(ip);
		return *this;
	}

	// Returns name of the computer
	string Name() const
	{
		return m_name;
	}

	// Prints computer information
	void print(ostream & os, int first = -1) const
	{
		os << "Host: " << m_name;
		for (size_t i = 0; i < m_adresses.size(); i++)
		{
			os << "\n";
			if (first == 1) os << "| ";
			else if (first == 0) os << "  ";

			os << "+-";
			os << m_adresses[i];
		}

		for (size_t i = 0; i < m_components.size(); i++)
		{
			os << "\n";
			bool arg2;
			if (first == 1) os << "| ";
			else if (first == 0) os << "  ";

			if (i == m_components.size() - 1)
			{
				arg2 = false;
				os << "\\-";
			}
			else
			{
				arg2 = true;
				os << "+-";
			}

			m_components[i]->print(os, first, arg2);
		}
	}

	// Overloaded operator << 
	friend ostream & operator << (ostream & os, const CComputer & comp)
	{
		comp.print(os);
		os << "\n";
		return os;
	}

private:
	string m_name;										//Computer name
	vector<string> m_adresses;							//List of computer ip addresses
	vector<shared_ptr<CComponent>> m_components;		//Computer components list
};



/*
	Class CNetwork represents
	connected group of computers
*/

class CNetwork
{
public:
	// Constructor
	CNetwork(const string & name) : m_name(name) {}

	//Copy constructor
	CNetwork(const CNetwork & net)
	{
		m_name = net.m_name;
		for (size_t i = 0; i < net.m_network2.size(); i++)
		{
			shared_ptr<CComputer> ptr = make_shared<CComputer>((*net.m_network2[i]));
			m_network1.emplace(ptr->Name(), ptr);
			m_network2.push_back(ptr);
		}
	}


	//Operator =
	CNetwork & operator = (const CNetwork & net)
	{
		if (this == &net) return *this;

		m_name = net.m_name;

		m_network1.clear();
		m_network2.clear();
		for (size_t i = 0; i < net.m_network2.size(); i++)
		{
			shared_ptr<CComputer> ptr = make_shared<CComputer>((*net.m_network2[i]));
			m_network1.emplace(ptr->Name(), ptr);
			m_network2.push_back(ptr);
		}

		return *this;
	}

	// Adds computer to the network
	CNetwork & AddComputer(const CComputer & comp)
	{
		string name = comp.Name();
		shared_ptr<CComputer> ptr = make_shared<CComputer>(comp);

		auto it = m_network1.find(name);
		if (it == m_network1.end())
		{
			m_network1.emplace(name, ptr);
			m_network2.push_back(ptr);
		}

		return *this;
	}

	// Finds computer by name and returns pointer to it
	shared_ptr<CComputer> FindComputer(const string & name) const
	{
		auto it = m_network1.find(name);
		if (it == m_network1.end()) return NULL;
		return it->second;
	}

	// Overloaded operator << 
	friend ostream & operator << (ostream & os, const CNetwork & net)
	{
		int arg2;
		os << "Network: " << net.m_name;
		for (size_t i = 0; i < net.m_network2.size(); i++)
		{
			os << "\n";

			if (i == net.m_network2.size() - 1)
			{
				os << "\\-";
				arg2 = 0;
			}
			else {
				os << "+-";
				arg2 = 1;
			}

			net.m_network2[i]->print(os, arg2);
		}
		os << "\n";
		return os;
	}

private:
	string m_name;										//Network name
	map<string, shared_ptr<CComputer>> m_network1;		//Map to fast search
	vector<shared_ptr<CComputer>> m_network2;			//List of computers in order they were added
};




/*
   Class CCPU represents computer CPU
*/

class CCPU : public CComponent
{
public:
	// Constructor
	CCPU(size_t cores, size_t freq) : m_cores(cores), m_freq(freq) {}

	// Copy constructor
	CCPU(const CCPU & ccpu)
	{
		m_cores = ccpu.m_cores;
		m_freq = ccpu.m_freq;
	}

	// Method to copy computer component (CPU)
	shared_ptr<CComponent> copy() const { return make_shared<CCPU>(*this); }

	// Prints CPU properties to the output stream 
	void print(ostream & os, int first, bool second) const
	{
		os << "CPU, " << m_cores << " cores @ " << m_freq << "MHz";
	}

private:
	size_t m_cores;		//Core count
	size_t m_freq;		//Core frequency
};




/*
	Class represents
	computer RAM
*/

class CMemory : public CComponent
{
public:
	// Constructor
	CMemory(size_t size) : m_size(size) {}

	// Copy constructor
	CMemory(const CMemory & mem)
	{
		m_size = mem.m_size;
	}

	// Method to copy computer component (Memory)
	shared_ptr<CComponent> copy() const { return make_shared<CMemory>(*this); }

	// Prints memory properties to the output stream 
	void print(ostream & os, int first, bool second) const
	{
		os << "Memory, " << m_size << " MiB";
	}

private:
	size_t m_size;		//Memory size
};




/*
	Class represents data storage
*/

class CDisk : public CComponent
{
public:
	// Contant defines disk type (SSD or HDD)
	static const int SSD = 0;
	static const int MAGNETIC = 1;

	// Constructor
	CDisk(int type, size_t size) : m_type(type), m_size(size), m_free(size) {}

	// Copy constructor
	CDisk(const CDisk & disk)
	{
		m_type = disk.m_type;
		m_size = disk.m_size;
		m_free = disk.m_free;
		m_partitions = disk.m_partitions;
	}

	// Method to copy computer component (Disk)
	shared_ptr<CComponent> copy() const { return make_shared<CDisk>(*this); }

	// Adds partition to the disk
	CDisk & AddPartition(size_t size, const string & path)
	{
		// Check if it is enough space to make new partition
		if (size <= m_free)
		{
			m_partitions.push_back(Partition(size, path));
			m_free = m_free - size;
		}

		return *this;
	}

	// Prints disk properties to the output stream 
	void print(ostream & os, int first, bool second) const
	{
		if (m_type == 0) os << "SSD, ";
		else os << "HDD, ";
		os << m_size << " GiB";

		for (size_t i = 0; i < m_partitions.size(); i++)
		{
			os << "\n";
			if (first == 1) os << "| ";
			else if (first == 0) os << "  ";

			if (second) os << "| ";
			else os << "  ";

			if (i == m_partitions.size() - 1) os << "\\-";
			else os << "+-";

			os << "[" << i << "]: " << m_partitions[i].m_size << " GiB, " << m_partitions[i].m_path;
		}
	}

private:

	/*
		Class represents computer disk partition
	*/

	class Partition
	{
	public:
		Partition(size_t size, const string & path) : m_size(size), m_path(path) {}
		size_t m_size;
		string m_path;
	};

	int m_type;							//Disk type
	size_t m_size;						//Disk size
	size_t m_free;						//Disk free space
	vector<Partition> m_partitions;		//List of disk partitions
};

#ifndef __PROGTEST__
template<typename _T>
string toString(const _T & x)
{
	ostringstream oss;
	oss << x;
	return oss.str();
}

int main(void)
{
	CNetwork n("FIT network");
	n.AddComputer(
		CComputer("progtest.fit.cvut.cz") .
		AddAddress("147.32.232.142") .
		AddComponent(CCPU(8, 2400)) .
		AddComponent(CCPU(8, 1200)) .
		AddComponent(CDisk(CDisk::MAGNETIC, 1500) .
			AddPartition(50, "/") .
			AddPartition(5, "/boot").
			AddPartition(1000, "/var")) .
		AddComponent(CDisk(CDisk::SSD, 60) .
			AddPartition(60, "/data")) .
		AddComponent(CMemory(2000)).
		AddComponent(CMemory(2000))) .
		AddComputer(
			CComputer("edux.fit.cvut.cz") .
			AddAddress("147.32.232.158") .
			AddComponent(CCPU(4, 1600)) .
			AddComponent(CMemory(4000)).
			AddComponent(CDisk(CDisk::MAGNETIC, 2000) .
				AddPartition(100, "/")   .
				AddPartition(1900, "/data"))) .
		AddComputer(
			CComputer("imap.fit.cvut.cz") .
			AddAddress("147.32.232.238") .
			AddComponent(CCPU(4, 2500)) .
			AddAddress("2001:718:2:2901::238") .
			AddComponent(CMemory(8000)));

	assert(toString(n) ==
		"Network: FIT network\n"
		"+-Host: progtest.fit.cvut.cz\n"
		"| +-147.32.232.142\n"
		"| +-CPU, 8 cores @ 2400MHz\n"
		"| +-CPU, 8 cores @ 1200MHz\n"
		"| +-HDD, 1500 GiB\n"
		"| | +-[0]: 50 GiB, /\n"
		"| | +-[1]: 5 GiB, /boot\n"
		"| | \\-[2]: 1000 GiB, /var\n"
		"| +-SSD, 60 GiB\n"
		"| | \\-[0]: 60 GiB, /data\n"
		"| +-Memory, 2000 MiB\n"
		"| \\-Memory, 2000 MiB\n"
		"+-Host: edux.fit.cvut.cz\n"
		"| +-147.32.232.158\n"
		"| +-CPU, 4 cores @ 1600MHz\n"
		"| +-Memory, 4000 MiB\n"
		"| \\-HDD, 2000 GiB\n"
		"|   +-[0]: 100 GiB, /\n"
		"|   \\-[1]: 1900 GiB, /data\n"
		"\\-Host: imap.fit.cvut.cz\n"
		"  +-147.32.232.238\n"
		"  +-2001:718:2:2901::238\n"
		"  +-CPU, 4 cores @ 2500MHz\n"
		"  \\-Memory, 8000 MiB\n");
	CNetwork x = n;
	auto c = x.FindComputer("imap.fit.cvut.cz");
	assert(toString(*c) ==
		"Host: imap.fit.cvut.cz\n"
		"+-147.32.232.238\n"
		"+-2001:718:2:2901::238\n"
		"+-CPU, 4 cores @ 2500MHz\n"
		"\\-Memory, 8000 MiB\n");
	c->AddComponent(CDisk(CDisk::MAGNETIC, 1000) .
		AddPartition(100, "system") .
		AddPartition(200, "WWW") .
		AddPartition(700, "mail"));
	assert(toString(x) ==
		"Network: FIT network\n"
		"+-Host: progtest.fit.cvut.cz\n"
		"| +-147.32.232.142\n"
		"| +-CPU, 8 cores @ 2400MHz\n"
		"| +-CPU, 8 cores @ 1200MHz\n"
		"| +-HDD, 1500 GiB\n"
		"| | +-[0]: 50 GiB, /\n"
		"| | +-[1]: 5 GiB, /boot\n"
		"| | \\-[2]: 1000 GiB, /var\n"
		"| +-SSD, 60 GiB\n"
		"| | \\-[0]: 60 GiB, /data\n"
		"| +-Memory, 2000 MiB\n"
		"| \\-Memory, 2000 MiB\n"
		"+-Host: edux.fit.cvut.cz\n"
		"| +-147.32.232.158\n"
		"| +-CPU, 4 cores @ 1600MHz\n"
		"| +-Memory, 4000 MiB\n"
		"| \\-HDD, 2000 GiB\n"
		"|   +-[0]: 100 GiB, /\n"
		"|   \\-[1]: 1900 GiB, /data\n"
		"\\-Host: imap.fit.cvut.cz\n"
		"  +-147.32.232.238\n"
		"  +-2001:718:2:2901::238\n"
		"  +-CPU, 4 cores @ 2500MHz\n"
		"  +-Memory, 8000 MiB\n"
		"  \\-HDD, 1000 GiB\n"
		"    +-[0]: 100 GiB, system\n"
		"    +-[1]: 200 GiB, WWW\n"
		"    \\-[2]: 700 GiB, mail\n");

	assert(toString(n) ==
		"Network: FIT network\n"
		"+-Host: progtest.fit.cvut.cz\n"
		"| +-147.32.232.142\n"
		"| +-CPU, 8 cores @ 2400MHz\n"
		"| +-CPU, 8 cores @ 1200MHz\n"
		"| +-HDD, 1500 GiB\n"
		"| | +-[0]: 50 GiB, /\n"
		"| | +-[1]: 5 GiB, /boot\n"
		"| | \\-[2]: 1000 GiB, /var\n"
		"| +-SSD, 60 GiB\n"
		"| | \\-[0]: 60 GiB, /data\n"
		"| +-Memory, 2000 MiB\n"
		"| \\-Memory, 2000 MiB\n"
		"+-Host: edux.fit.cvut.cz\n"
		"| +-147.32.232.158\n"
		"| +-CPU, 4 cores @ 1600MHz\n"
		"| +-Memory, 4000 MiB\n"
		"| \\-HDD, 2000 GiB\n"
		"|   +-[0]: 100 GiB, /\n"
		"|   \\-[1]: 1900 GiB, /data\n"
		"\\-Host: imap.fit.cvut.cz\n"
		"  +-147.32.232.238\n"
		"  +-2001:718:2:2901::238\n"
		"  +-CPU, 4 cores @ 2500MHz\n"
		"  \\-Memory, 8000 MiB\n");

	return 0;
}
#endif /* __PROGTEST__ */
