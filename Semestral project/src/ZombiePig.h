#ifndef ZombiePig_H
#define ZombiePig_H

#include <SDL2/SDL.h>

#include <vector>

#include "Monster.h"
#include "Item.h"

/*!
 * Class represents zombie-pig
 */
class ZombiePig : public Monster
{
public:
	/*!
	 * Constructor
	 * @param x X-Position
	 * @param y Y-Position
	 * @param render Surface to draw
	 */
	ZombiePig(int x, int y, SDL_Surface * render);
	
	/*!
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	shared_ptr<Item> getDrop();

	/*!
	 * Creates string representation of a zombie-pig
	 * @return Returns string representation of a zombie-pig
	 */
	string toString();
};

#endif
