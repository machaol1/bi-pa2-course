#include "Blocks.h"

Grass::Grass(int x, int y, SDL_Surface * render) : Object(TextureManager::grassSurface, render, 1, x, y, 80, 80) { }
void Grass::draw(int time, int offsetX, int offsetY, shared_ptr<Map> m)
{
	SDL_Rect dst;
	dst.x = pos.x - offsetX;
	dst.y = pos.y - offsetY;
	dst.w = 80;
	dst.h = 80;
	TextureManager::Render(r, s, NULL, &dst);
}

shared_ptr<Item> Grass::getDrop() { return make_shared<Item>("Dirt", 1, r); }

string Grass::toString()
{
	string result;
	result = "G:" + getInfo();
	return result;
}


Dirt::Dirt(int x, int y, SDL_Surface * render) : Object(TextureManager::dirtSurface, render, 1, x, y, 80, 80) { }
void Dirt::draw(int time, int offsetX, int offsetY, shared_ptr<Map> m)
{
	SDL_Rect dst;
	dst.x = pos.x - offsetX;
	dst.y = pos.y - offsetY;
	dst.w = 80;
	dst.h = 80;
	TextureManager::Render(r, s, NULL, &dst);
}

shared_ptr<Item> Dirt::getDrop() { return make_shared<Item>("Dirt", 1, r); }

string Dirt::toString()
{
	string result;
	result = "D:" + getInfo();
	return result;
}


Stone::Stone(int x, int y, SDL_Surface * render) : Object(TextureManager::stoneSurface, render, 1, x, y, 80, 80) { }
void Stone::draw(int time, int offsetX, int offsetY, shared_ptr<Map> m)
{
	SDL_Rect dst;
	dst.x = pos.x - offsetX;
	dst.y = pos.y - offsetY;
	dst.w = 80;
	dst.h = 80;
	TextureManager::Render(r, s, NULL, &dst);
}

shared_ptr<Item> Stone::getDrop() { return make_shared<Item>("Stone", 1, r); }

string Stone::toString()
{
	string result;
	result = "S:" + getInfo();
	return result;
}



Gold::Gold(int x, int y, SDL_Surface * render) : Object(TextureManager::goldSurface, render, 1, x, y, 80, 80) { }
void Gold::draw(int time, int offsetX, int offsetY, shared_ptr<Map> m)
{
	SDL_Rect dst;
	dst.x = pos.x - offsetX;
	dst.y = pos.y - offsetY;
	dst.w = 80;
	dst.h = 80;
	TextureManager::Render(r, s, NULL, &dst);
}

shared_ptr<Item> Gold::getDrop() { return make_shared<Item>("Gold", 1, r); }

string Gold::toString()
{
	string result;
	result = "Z:" + getInfo();
	return result;
}



Redstone::Redstone(int x, int y, SDL_Surface * render) : Object(TextureManager::redstoneSurface, render, 1, x, y, 80, 80) { }
void Redstone::draw(int time, int offsetX, int offsetY, shared_ptr<Map> m)
{
	SDL_Rect dst;
	dst.x = pos.x - offsetX;
	dst.y = pos.y - offsetY;
	dst.w = 80;
	dst.h = 80;
	TextureManager::Render(r, s, NULL, &dst);
}

shared_ptr<Item> Redstone::getDrop() { return make_shared<Item>("Redstone", 1, r); }

string Redstone::toString()
{
	string result;
	result = "R:" + getInfo();
	return result;
}


Coal::Coal(int x, int y, SDL_Surface * render) : Object(TextureManager::coalSurface, render, 1, x, y, 80, 80) { }
void Coal::draw(int time, int offsetX, int offsetY, shared_ptr<Map> m)
{
	SDL_Rect dst;
	dst.x = pos.x - offsetX;
	dst.y = pos.y - offsetY;
	dst.w = 80;
	dst.h = 80;
	TextureManager::Render(r, s, NULL, &dst);
}

shared_ptr<Item> Coal::getDrop() { return make_shared<Item>("Coal", 1, r); }

string Coal::toString()
{
	string result;
	result = "C:" + getInfo();
	return result;
}

Leaves::Leaves(int x, int y, SDL_Surface * render) : Object(TextureManager::leavesSurface, render, 1, x, y, 80, 80) { }
void Leaves::draw(int time, int offsetX, int offsetY, shared_ptr<Map> m)
{
	SDL_Rect dst;
	dst.x = pos.x - offsetX;
	dst.y = pos.y - offsetY;
	dst.w = 80;
	dst.h = 80;
	TextureManager::Render(r, s, NULL, &dst);
}

shared_ptr<Item> Leaves::getDrop() { return NULL; }

string Leaves::toString()
{
	string result;
	result = "L:" + getInfo();
	return result;
}
