#include "Inventory.h"

Inventory::Inventory()
{
	inv.resize(6);
}

size_t Inventory::findIndex(string name)
{
	for (size_t i = 0; i < inv.size(); i++)
	{
		if (inv[i] == NULL) continue;
		if (inv[i]->getName() == name) return i;
	}

	return -1;
}

size_t Inventory::findFree()
{
	for (size_t i = 0; i < inv.size(); i++)
		if (inv[i] == NULL) return i;

	return -1;
}

bool Inventory::insert(shared_ptr<Item> it)
{
	if (it == NULL) return false;
	int i = findIndex(it->getName());
	if (i == -1)
	{
		int free = findFree();
		if (free == -1) return false;
		inv[free] = it;
	}
	else {
		inv[i]->setCount(inv[i]->getCount() + 1);
	}

	return true;
}

string Inventory::toString()
{
	string result;
	for (int i = 0; i < 6; i++)
	{
		if (inv[i] == NULL) result = result + "0\n";
		else result = result + inv[i]->toString() + "\n";
	}
	return result;
}

shared_ptr<Item> Inventory::getItem(size_t i)
{
	if (i < 0 || i > 5) return NULL;
	return inv[i];
}

void Inventory::draw(size_t selectedItem, SDL_Surface * render)
{
	SDL_Surface * t;
	for (size_t i = 0; i < 6; i++)
	{
		if (i == selectedItem) t = TextureManager::selectedSurface;
		else t = TextureManager::notSelectedSurface;

		SDL_Rect dst;
		dst.x = 95 + i * 110;
		dst.y = 530;
		dst.h = 90;
		dst.w = 90;

		TextureManager::Render(render, t, NULL , &dst);


		if (i < inv.size() && inv[i] != NULL)
		{
			if (inv[i]->getCount() == 0)
			{
				inv[i] == NULL;
				continue;
			}

			SDL_Surface * t1 = inv[i]->getSprite();
			SDL_Rect dst1;
			dst1.x = 105 + i * 110;
			dst1.y = 540;
			dst1.w = 80;
			dst1.h = 80;
	
			TextureManager::Render(render, t1, NULL, &dst1);
		}
	}
}
