#ifndef Game_H
#define Game_H

#include "Map.h"
#include "Object.h"
#include "Movable.h"
#include "Inventory.h"
#include "Item.h"
#include "Zombie.h"
#include "ZombiePig.h"
#include "Generator.h"
#include "Blocks.h"
#include "Player.h"

#include <memory>
#include <vector>


/*!
* Main class which represents whole game
*/
class Game
{
public:

	/*!
	* Constructor
	*/
	Game();

	/*!
	* Method saves current gamestate
	* @return Returns true if succeeded
	*/
	bool save();

	/*!
	* Method loads gamestate from savefile
	* @return Returns true if succeeded
	*/
	bool load();


	/*!
	* Method will dealocate memory, free texutures, destroy window
	*/
	void clean();

	/*!
	* Method parses string by delimiter
	* @return Returns array of string which were parsed
	*/
	vector<string> parse(string str, char delimiter);
	
	/*!
	* Method implements user left click
	*/
	void leftClick();

	/*!
	* Method implements user right click
	*/
	void rightClick();

	/*!
	* Main game loop
	*/
	void start();

protected:
	int offsetX, offsetY;				/*!< Current offset */
	SDL_Window * window;				/*!< Main game window */
	SDL_Surface * windowSurface;		/*!< Game window surface */

	shared_ptr<Player> p;				/*!< Player */
	shared_ptr<Map> map;				/*!< Game map */
	vector<shared_ptr<Monster>> enemy;	/*!< Array of enemys */
	Generator gen;						/*!< Map generator */
};

#endif