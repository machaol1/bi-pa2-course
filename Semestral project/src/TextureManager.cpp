#include "TextureManager.h"

SDL_Surface * TextureManager::LoadTexture(const char * filename)
{
	SDL_Surface * tmp = SDL_LoadBMP(filename);
	return tmp;
}

void TextureManager::Render(SDL_Surface * window, SDL_Surface * tex, SDL_Rect * src, SDL_Rect * dst)
{
	SDL_BlitSurface(tex, src, window, dst);
}

bool TextureManager::Intersection(SDL_Rect a, SDL_Rect b)
{	
	int tlax = a.x;
	int tlay = a.y;
	
	int brax = a.x + a.w;
	int bray = a.y + a.h;

	int tlbx = b.x;
	int tlby = b.y;

	int brbx = b.x + b.w;
	int brby = b.y + b.h;

	if (brax < tlbx || brbx < tlax || bray < tlby || brby < tlay) return false;
	return true;
}

SDL_Surface * TextureManager::dirtSurface = TextureManager::LoadTexture("src/mine/dirt.bmp");
SDL_Surface * TextureManager::grassSurface = TextureManager::LoadTexture("src/mine/grass.bmp");
SDL_Surface * TextureManager::goldSurface = TextureManager::LoadTexture("src/mine/gold.bmp");
SDL_Surface * TextureManager::redstoneSurface = TextureManager::LoadTexture("src/mine/redstone.bmp");
SDL_Surface * TextureManager::stoneSurface = TextureManager::LoadTexture("src/mine/stone.bmp");
SDL_Surface * TextureManager::coalSurface = TextureManager::LoadTexture("src/mine/coal.bmp");
SDL_Surface * TextureManager::leavesSurface = TextureManager::LoadTexture("src/mine/leaves.bmp");
SDL_Surface * TextureManager::zombieSurface = TextureManager::LoadTexture("src/player/zombie.bmp");
SDL_Surface * TextureManager::zombiePigSurface = TextureManager::LoadTexture("src/player/zombie-pig.bmp");
SDL_Surface * TextureManager::playerSurface = TextureManager::LoadTexture("src/player/ded.bmp");
SDL_Surface * TextureManager::hearthSurface = TextureManager::LoadTexture("src/player/hearth.bmp");
SDL_Surface * TextureManager::selectedSurface = TextureManager::LoadTexture("src/player/selectedItem.bmp");
SDL_Surface * TextureManager::notSelectedSurface = TextureManager::LoadTexture("src/player/notSelectedItem.bmp");
