#ifndef Object_H
#define Object_H

#include <SDL2/SDL.h>
#include <vector>
#include <memory>
#include <sstream>

#include "TextureManager.h"
using namespace std;

class Map;
class Item;

/*!
 * Class represents object
 */
class Object
{
public:

	/*!
	 * Constructor
	 * @param surface Object surface
	 * @param render Surface to draw
	 * @param hp Object hp
	 * @param x X-Positon
	 * @param y Y-Position
	 * @param w Object width
	 * @param h Object height
	 */
	Object(SDL_Surface * surface, SDL_Surface * render, int hp, int x, int y, int w, int h);

	/*!
	 * Indicates if object is not killed/destroyed
	 * @return Returns true if object hp > 0
	 */
	bool isAlive();

	/*!
	 * Gets object position
	 * @return Returns object position
	 */
	SDL_Rect getPos();

	/*!
	 * Method for getting damage
	 * @param value Damage to get
	 */
	virtual void getDmg(int value);


	/*!
	 * Method gets current hp
	 * @return Returns hp
	 */
	int getHP();


	/*!
	 * Gets position and hp as a string. Is used by toString()
	 * @return String which contains coordinates and hp
	 */
	string getInfo();

	/*!
	 * Pure vitrual method
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	virtual shared_ptr<Item> getDrop() = 0;

	/*!
	 * Pure vitrual method
	 * Method draws object on screen
	 * @param time Past time
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */
	virtual void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m) = 0;
	
	/*!
	* Pure virtual method
	* Creates string representation of an object
	* @return Returns string representation of an object
	*/
	virtual string toString() = 0;

protected:
	SDL_Surface * s;		/*!< Object surface */
	SDL_Surface * r;		/*!< Surface to draw */
	SDL_Rect pos;			/*!< Object position */
	SDL_Rect src;			/*!< Surface source rectangle */
	SDL_Rect dst;			/*!< Surface destination rectangle */
	int hp;					/*!< Object health */
	bool alive;				/*!< Indicator of object life */
};

#endif
