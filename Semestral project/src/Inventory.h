#ifndef Inventory_H
#define Inventory_H

#include <vector>

#include "Item.h"


/*!
 * Class represents player inventory
 */
class Inventory
{
public:
	/*!
	 * Constructor
	 */
	Inventory();

	/*!
	 * Method finds free inventory place
	 * @return Returns index of free place in inventory or -1 if inventory is full
	 */
	size_t findFree();

	/*!
	 * Method finds index of item in inventory
	 * @param name Item name
	 * @return Returns index of item in inventory or -1 if not found
	 */
	size_t findIndex(string name);


	/*!
	 * Method returns item on current index
	 * @param i Index of the item
	 * @return Returns item
	 */
	shared_ptr<Item> getItem(size_t i);

	/*!
	 * Inserts item to the inventory
	 * @param it Item to insert
	 * @return Returns true if succeded
	 */
	bool insert(shared_ptr<Item> it);

	/*!
	 * Draws inventory panel on screen
	 * @param selectedItem Item which is selected
	 * @param render Surface to draw
	 */
	void draw(size_t selectedItem, SDL_Surface * render);

	/*!
	* Creates string representation of inventory
	* @return Returns string representation of inventory
	*/
	string toString();

protected:
	vector<shared_ptr<Item>> inv; /*!< Item array */
};

#endif
