#include <string>
#include <iostream>
#include <fstream>
#include <deque>
#include <ctime>
#include <cstdlib>
#include <sstream>
#include "Game.h"

using namespace std;

int main(int argc, char *argv[])
{
	Game game;
	game.start();
	return 0;
}